package sbu.cs;

public class Parse {
    private String input;
    private Object Green;

    public Parse(String input) {
        this.input = input;
    }
    public String parser(Matrix in) {
        int n = in.getMatrix().length;
        ((Green) in.getMatrix()[0][0]).setIn(input);
        for(int i = 0 ; i < n ; i++) {
            for(int j = 0 ; j < n ; j++) {
                if(in.getColor()[i][j].equals("green")) {
                    ((Green) in.getMatrix()[i][j]).setOut();
                    String temp = ((Green) in.getMatrix()[i][j]).getOut();
                    switch (in.getColor()[i+1][j]) {//paen
                        case "green" :
                            ((Green) in.getMatrix()[i+1][j]).setIn(temp);
                            break;
                        case "blue" :
                            ((Blue) in.getMatrix()[i+1][j]).setInUp(temp);
                            break;
                        case "yellow" :
                            ((Yellow) in.getMatrix()[i+1][j]).setIn(temp);
                            break;
                    }
                    switch (in.getColor()[i][j+1]) {
                        case "green" :
                            ((Green) in.getMatrix()[i][j+1]).setIn(temp);
                            break;
                        case "blue" :
                            ((Blue) in.getMatrix()[i][j+1]).setInLeft(temp);
                            break;
                        case "yellow" :
                            ((Yellow) in.getMatrix()[i][j+1]).setIn(temp);
                            break;
                    }
                }
                else if(in.getColor()[i][j].equals("yellow")) {
                    ((Yellow) in.getMatrix()[i][j]).setOut();
                    String temp = ((Yellow) in.getMatrix()[i][j]).getOut();
                    if(i == n-1) {
                        ((Pink) in.getMatrix()[i][j+1]).setLeftIn(temp);
                    }
                    else {
                        ((Pink) in.getMatrix()[i+1][j]).setupIn(temp);
                    }
                }
                else if(in.getColor()[i][j].equals("blue")) {
                    if(in.getColor()[i+1][j].equals("pink")) {
                        ((Blue) in.getMatrix()[i][j]).setOutDown();
                        String temp = ((Blue) in.getMatrix()[i][j]).getOutDown();
                        ((Pink) in.getMatrix()[i+1][j]).setupIn(temp);
                    }
                    else if(in.getColor()[i][j+1].equals("pink")) {
                        ((Blue) in.getMatrix()[i][j]).setOutRight();
                        String temp = ((Blue) in.getMatrix()[i][j]).getOutRight();
                        System.out.println(temp);
                        ((Pink) in.getMatrix()[i][j+1]).setLeftIn(temp);
                    }
                    else if(in.getColor()[i+1][j].equals("blue")) {
                        ((Blue) in.getMatrix()[i][j]).setOutDown();
                        String temp = ((Blue) in.getMatrix()[i][j]).getOutDown();
                        ((Blue) in.getMatrix()[i+1][j]).setInUp(temp);
                    }
                    else {
                        ((Blue) in.getMatrix()[i][j]).setOutRight();
                        String temp = ((Blue) in.getMatrix()[i][j]).getOutRight();
                        ((Blue) in.getMatrix()[i][j+1]).setInLeft(temp);
                    }
                }
                else {
                    ((Pink) in.getMatrix()[i][j]).setOut();
                    String temp = ((Pink) in.getMatrix()[i][j]).getOut();
                    if(i == n-1 && j != n-1) {
                        ((Pink) in.getMatrix()[i][j+1]).setLeftIn(temp);
                    }
                    else if(i != n-1 && j == n-1){
                        ((Pink) in.getMatrix()[i+1][j]).setupIn(temp);
                    }
                }
            }
            ((Pink) in.getMatrix()[n-1][n-1]).setOut();
        }
        return ((Pink) in.getMatrix()[n-1][n-1]).getOut();
    }
}
