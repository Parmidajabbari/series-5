package sbu.cs;

public class White {

    WhiteFunction One = (arg1, arg2) -> {
        if(arg1 == null || arg2 == null) {
            return "";
        }
        String mix = "";
        int i = 0;
        int j = 0;
        while(i < arg1.length() && j < arg2.length()) {
            mix = mix + arg1.charAt(i) + arg2.charAt(j);
            i++;
            j++;
        }
        while(i < arg1.length()) {
            mix = mix + arg1.charAt(i);
            i++;
        }
        while(j < arg2.length()) {
            mix = mix + arg2.charAt(j);
            j++;
        }
        return mix;
    };
    WhiteFunction Two = (arg1, arg2) -> {
        Black reverse = new Black();
        return arg1 + reverse.One.func(arg2);
    };
    WhiteFunction Three = (arg1, arg2) -> {
        String mix = "";
        int i = 0;
        int j = arg2.length() - 1;
        while(i < arg1.length() && j >= 0) {
            mix = mix + arg1.charAt(i) + arg2.charAt(j);
            i++;
            j--;
        }
        while(i < arg1.length()) {
            mix = mix + arg1.charAt(i);
            i++;
        }
        while(j >= 0) {
            mix = mix + arg2.charAt(j);
            j--;
        }
        return mix;
    };
    WhiteFunction Four = (arg1, arg2) -> {
        if (arg1 == null) {
            return "";
        }
        int i = arg1.length() % 2;
        if(i == 0) {
            return arg1;
        }
        return arg2;
    };
    WhiteFunction Five = (arg1, arg2) -> {
        if(arg1 == null || arg2 == null) {
            return "u";
        }
        int i = 0;
        int j = 0;
        String New = "";
        while (i < arg1.length() && j < arg2.length()) {
            int temp1 = arg1.charAt(i) - 97;
            int temp2 = arg2.charAt(j) - 97;
            char sum =(char)(((temp1+temp2) % 26) + 97);
            New = New + sum;
            i++;
            j++;
        }
        while (i < arg1.length()) {
            New = New + arg1.charAt(i);
            i++;
        }
        while (j < arg2.length()) {
            New = New + arg2.charAt(j);
            j++;
        }
        return New;
    };
}

