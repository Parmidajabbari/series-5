package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n , int[][] arr , String input) {
        Matrix matrix = new Matrix(n);
        matrix.setMatrix(arr);
        Parse parse = new Parse(input);
        return parse.parser(matrix);
    }
}
