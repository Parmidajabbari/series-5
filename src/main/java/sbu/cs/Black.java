package sbu.cs;

public class Black {
    BlackFunction One = arg -> {
        if(arg == null) {
            return "";
        }
        StringBuilder Reverse = new StringBuilder(arg);
        Reverse.reverse();
        return Reverse.toString();
    };
    BlackFunction Two = arg -> {
        String Duplicate = "";
        for(int i = 0 ; i < arg.length() ; i++) {
            Duplicate = Duplicate + arg.charAt(i) + arg.charAt(i);
        }
        return Duplicate;
    };
    BlackFunction Three = arg -> arg + arg;
    BlackFunction Four = arg -> {
        if(arg == null) {
            return "";
        }
        String shift = Character.toString(arg.charAt(arg.length() - 1));
        for(int i = 0 ; i < arg.length() - 1 ; i++) {
            shift = shift + arg.charAt(i);
        }
        return shift;
    };
    BlackFunction Five = arg -> {
        if(arg == null) {
            return "";
        }
        String New = "";
        for (int i = 0; i < arg.length(); i++) {
            int temp = arg.charAt(i);
            int position = 122 - (temp - 97);
            New = New + (char) position;
        }
        return New;

    };
}

