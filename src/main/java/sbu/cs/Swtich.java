package sbu.cs;

public class Swtich {
    private int n;
    public Swtich(int n) {
        this.n = n;
    }
    public String black(String arg) {
        Black New = new Black();
        switch (n) {
            case 1 :
                return New.One.func(arg);
            case 2 :
                return New.Two.func(arg);
            case 3 :
                return New.Three.func(arg);
            case 4 :
                return New.Four.func(arg);
            case 5 :
                return New.Five.func(arg);
        }
        return null;
    }
    public String white(String arg1 , String arg2) {
        White New = new White();
        switch (n) {
            case 1 :
                return New.One.func(arg1 , arg2);
            case 2 :
                return New.Two.func(arg1 , arg2);
            case 3 :
                return New.Three.func(arg1 , arg2);
            case 4 :
                return New.Four.func(arg1 , arg2);
            case 5 :
                return New.Five.func(arg1 , arg2);
        }
        return null;
    }
}
