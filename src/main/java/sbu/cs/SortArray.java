package sbu.cs;


public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int Minimum(int[] arr , int size , int t) {
        int minIndex = t;
        for(int i = t ; i < size ; i++) {
            if(arr[i] < arr[minIndex]) {
                minIndex = i;
            }
        }
        return minIndex;
    }
    public int[] selectionSort(int[] arr, int size) {
        for(int i = 0 ; i < size ; i++) {
            int index = Minimum(arr,size,i);
            int temp = arr[index];
            arr[index] = arr[i];
            arr[i] = temp;

        }
        return arr;
    }


    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for(int i = 1 ; i < size ; i++) {
            int s = i;
            for(int j = i - 1 ; j >= 0 ; j--) {
                if(arr[s] < arr[j]) {
                    int temp = arr[s];
                    arr[s] = arr[j];
                    arr[j] = temp;
                    s = j;
                }
            }
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        sort(arr , 0 , size - 1);
        return arr;
    }
    public void merge(int[] arr , int l , int r , int middle) {
        int[] tempRight = new int[r - middle];
        int[] tempLeft = new int[middle - l + 1];
        for (int i = 0; i <  middle - l + 1; i++) {
             tempLeft[i] = arr[l + i];
         }
        for (int j = 0; j < r - middle; j++) {
            tempRight[j] = arr[middle + 1 + j];
        }
        int i = 0;
        int j = 0;
        int k = l;
        while (i < middle - l + 1 && j < r - middle) {
            if (tempLeft[i] <= tempRight[j]) {
                arr[k] = tempLeft[i];
                i++;
            }
            else {
                arr[k] = tempRight[j];
                j++;
            }
            k++;
        }
        while (i < middle - l + 1) {
            arr[k] = tempLeft[i];
            i++;
            k++;
        }
        while (j < r - middle) {
            arr[k] = tempRight[j];
            j++;
            k++;
        }
    }

    public void sort(int[] arr , int l , int r) {
        if(l >= r) {
            return;
        }
        int middle  = l + (r - l) / 2;
        sort(arr,l,middle);
        sort(arr,middle+1,r);
        merge(arr,l,r,middle);
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int r = arr.length - 1;
        int l = 0;
        while(r - l + 1 != 1) {
            int middle = (r + l) / 2;
            if(arr[middle] == value) {
                return middle;
            }
            int size = (r - l + 1) / 2;
            int leftOut = ((r-l) % 2);
            if(arr[middle - leftOut] > value) {
                r = middle - leftOut;
                l = middle - size - 1 + leftOut;
            }
            else {
                l = middle + leftOut;
                r = middle + size;
            }
        }
        return -1;
    }


    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        return Recursive(arr , value , 0 , arr.length - 1);
    }
    public int Recursive(int [] arr , int value , int l , int r) {
        int middle = (r + l) / 2;
        if (r - l + 1 == 1)
        {
            return -1;
        }
        else if (arr[middle] == value) {
            return middle;
        }
        int size = (r - l + 1) / 2;
        int leftOut = ((r-l) % 2);
        if(arr[middle - leftOut] > value) {
            return Recursive(arr , value , middle - size - 1 + leftOut , middle - leftOut);
        }
        else {
            return Recursive(arr , value , middle + leftOut , middle + size);
        }
    }
}
