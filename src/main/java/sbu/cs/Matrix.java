package sbu.cs;


public class Matrix {
    private int n;
    private Color[][] matrix;
    private String[][] colors;
    public Matrix(int n) {
        this.n = n;
        this.matrix =  new Color[n][n];
        this.colors = new String[n][n];
    }
    public void setMatrix(int[][] arr) {
        for (int i = 0 ; i < n ; i++) {
            for (int j = 0 ; j < n ; j++) {
              int temp = arr[i][j];
              if((i == 0 && j == n-1) || (i == n-1 && j == 0)) {
                  Yellow Newyellow = new Yellow(temp);
                  matrix[i][j] = Newyellow;
                  colors[i][j] = "yellow";
              }
              else if((i == 0 && j < n-1) || (i < n-1 && j == 0)) {
                  Green Newgreen = new Green(temp);
                  matrix[i][j] = Newgreen;
                  colors[i][j] = "green";
              }
              else if((i != 0 && j == n-1) || (i == n-1 && j != 0)) {
                  Pink Newpink = new Pink(temp);
                  matrix[i][j] = Newpink;
                  colors[i][j] = "pink";
              }
              else {
                  Blue Newblue = new Blue(temp);
                  matrix[i][j] = Newblue;
                  colors[i][j] = "blue";
              }
            }
        }
    }

    public String[][] getColor() {
        return colors;
    }

    public Color[][] getMatrix() {
        return matrix;
    }
}
